/** 
*  user model
*  Describes the characteristics of each attribute in a user resource.
*
* @author Lahari Thamatam <S534689@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  email: { type: String, required: true, unique: true },
  userAccountNumber: { type: Number, required: true, unique: true },
  userBalance: { type: Number, required: true, default:'0' },
  address: { type: String, required: true, default: 'Address 1' },
  city: { type: String, required: true, default: 'Maryville' },
  state: { type: String, required: true, default: 'MO' },
  zip: { type: String, required: true, default: '64468' },
  country: { type: String, required: true, default: 'USA' }
})

module.exports = mongoose.model('User', UserSchema)
