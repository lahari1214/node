/** 
*  account model
*  Describes the characteristics of each attribute in an account item - one entry on a user's account.
*
* @author Nithya Vudayamarri <s534641@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const AccountSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  typeofAccount: { type: String, required: true },
  branch: { type: String, required: true },
  dateOpened: { type: Date, required: true, default: 1 },
  accountBalance: { type: Number, required: true },
})

module.exports = mongoose.model('Account', AccountSchema)
